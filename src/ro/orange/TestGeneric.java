package ro.orange;

public class TestGeneric {

    public static void main(String[] args) {
        // define 2 objects of Generic class, type String
        Generic <String> gen1 = new Generic<>("Samuel Jackson");
        Generic <String> gen2 = new Generic<>("Plaza hotel");

        System.out.println("My name is " + gen1.get() + ".");
        System.out.println("I work as a cashier at " + gen2.get() + ".");

        Generic<Integer> int1 = new Generic<>(2000);
        System.out.println("My salary is valued somewhere around " + int1.get() + " dollars.");


    }
}
